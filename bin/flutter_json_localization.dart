import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as path;
import 'package:yaml/yaml.dart';

import 'src/string_extension.dart';

String? _stringsPath;
String? _suportedLocales;
List<String> _filesName = [];
List<Clazz> _clazzes = [];
List<String> _clazzInstances = [];
List<MapContentData> _mapContentDataList = [];

class MapContentData {
  String clazzName;
  Map<String, Map<String, dynamic>> mapsList;
  Set<String> errorKeys;

  MapContentData(this.clazzName, this.mapsList, this.errorKeys);
}

class Clazz {
  String clazzName;
  String content;

  Clazz({
    required this.clazzName,
    required this.content,
  });
}

void _createMaps(Map yaml) async {
  final locales = (yaml["locales"] as List?)
      ?.map((locale) => "${locale["language-code"]}")
      .toList();
  locales?.forEach((locale) {
    var jsonPath = "${_path(yaml)}$locale";
    Directory dir = Directory(jsonPath);
    var files = dir.listSync(recursive: false);
    for (FileSystemEntity fileEntity in files) {
      final file = fileEntity as File;
      final clazzName =
          "${file.path.split(path.separator).last.split('.').first.capitalize()}";
      final String fileContent = file.readAsStringSync();
      try {
        Map<String, dynamic> fileContentMap = json.decode(fileContent);
        var existPos = _mapContentDataList
            .indexWhere((element) => element.clazzName == clazzName);
        if (existPos == -1) {
          _mapContentDataList
              .add(MapContentData(clazzName, {locale: fileContentMap}, Set()));
        } else {
          _mapContentDataList[existPos].mapsList[locale] = fileContentMap;
        }
      } on FormatException catch (_) {
        print(
            "GENERATION ERROR ${file.path.split('/').last} is in an invalid format");
        return;
      }
    }
  });
  _mapContentDataList.forEach((mapContentData) {
    Set<String> allKeys = Set();
    Set<String> errorKeys = Set();
    mapContentData.mapsList.forEach((key, value) {
      allKeys.addAll(value.keys);
    });
    allKeys.forEach((key) {
      mapContentData.mapsList.values.forEach((element) {
        if (!element.keys.contains(key)) {
          errorKeys.add(key);
        }
      });
    });
    mapContentData.errorKeys = errorKeys;
  });
}

void main(List<String> arguments) async {
  final yaml = _getConfigurations();
  _stringsPath = _path(yaml);
  _suportedLocales = _locales(yaml);

  final jsonPath = _jsonPath(yaml);

  Directory dir = Directory(jsonPath);
  _createMaps(yaml);
  var files = dir.listSync(recursive: false);

  for (FileSystemEntity fileEntity in files) {
    final file = fileEntity as File;

    final fileName = file.path.split(path.separator).last.split('.').first;
    final clazzName =
        "${file.path.split(path.separator).last.split('.').first.capitalize()}";

    final String fileContent = file.readAsStringSync();

    try {
      Map<String, dynamic> fileContentMap = json.decode(fileContent);

      final instance =
          "  static $clazzName get ${fileName.uncapitalize()} => $clazzName();";
      final clazz = Clazz(
          clazzName: clazzName, content: _clazz(clazzName, fileContentMap));
      _clazzInstances.add(instance);
      _clazzes.add(clazz);
      _filesName.add("    \"$fileName\"");
    } on FormatException catch (_) {
      print(
          "GENERATION ERROR ${file.path.split('/').last} is in an invalid format");
      return;
    }
  }

  var isErrorExist = false;
  _mapContentDataList.forEach((element) {
    if (element.errorKeys.isNotEmpty) {
      isErrorExist = true;
      print(
          "GENERATION ERROR ${element.clazzName} errors keys = ${element.errorKeys.join(", ")}");
    }
  });
  if (isErrorExist) return;

  final fileFullPath = _fileFullPath(yaml, "json_localization.gen.dart");

  final genFile = File(fileFullPath);
  var imports = _clazzes.map((e) => "import '${e.clazzName}.gen.dart';");
  if (genFile.existsSync()) {
    genFile.deleteSync();
  }
  var fileContent = "${imports.join("\n")}\n$_file";
  genFile.createSync(recursive: true);
  var sink = genFile.openWrite();
  sink.write(fileContent);
  await sink.flush();
  await sink.close();
  _createClazzes(yaml);
  print("json_localization GENERATION BUILD DONE!");
}

void _createClazzes(Map yaml) async {
  _clazzes.forEach((element) async {
    String header = '''
import 'dart:ui';
import 'package:flutter_json_localization/src/string_extension.dart';\n\n

//GENERATED CODE - DO NOT MODIFY BY HAND\n''';
    final fileFullPath = _fileFullPath(yaml, "${element.clazzName}.gen.dart");

    final genFile = File(fileFullPath);

    if (genFile.existsSync()) {
      genFile.deleteSync();
    }

    genFile.createSync(recursive: true);
    var sink = genFile.openWrite();

    sink.write(header += element.content);
    await sink.flush();
    await sink.close();
  });
}

Map _getConfigurations() {
  final File file = File('pubspec.yaml');
  final String yamlString = file.readAsStringSync();
  final Map yamlMap = loadYaml(yamlString);
  return yamlMap["flutter_json_localization"];
}

String _path(Map yaml) {
  final String path = yaml["path"];
  return path.endsWith("/") ? path : "$path/";
}

String _locales(Map yaml) {
  final locales = (yaml["locales"] as List?)
      ?.map(
        (locale) =>
            locale["countries-code"]
                ?.map(
                  (countryCode) =>
                      "    Locale('${locale["language-code"]}', '$countryCode')",
                )
                ?.toList() ??
            ["    Locale('${locale["language-code"]}')"],
      )
      .toList()
      .reduce(
        (l1, l2) =>
            List.castFrom<dynamic, String>(l1).toList() +
            List.castFrom<dynamic, String>(l2).toList(),
      );

  return "[\n${locales.join(',\n')},\n  ]";
}

String _jsonPath(Map yaml) {
  final language = yaml['locales'].first['language-code'];

  final country = yaml['locales']?.first['countries-code']?.first ?? "";

  return "${_path(yaml)}$language/$country";
}

String _clazz(String clazzName, Map<String, dynamic> content) {
  final keys = [];
  String forContent = "";
  var clazz = _mapContentDataList
      .firstWhere((element) => element.clazzName == clazzName);
  keys.add(
      '''//${clazz.errorKeys.isEmpty ? "all keys exist" : "errors keys = " + clazz.errorKeys.join(", ")}''');
  clazz.mapsList.forEach((key, value) {
    keys.add('''    
  static final $key = ${json.encode(value)};''');
    forContent += forContent = '''
     case '$key': 
       return $key;\n''';
  });

  forContent += '''
     default:
       return null;''';

  keys.add('''
   
  static Map<String, dynamic>? getMapByLocale(Locale locale){
    switch (locale.languageCode) {\n$forContent
     }
   }
   
   ''');

  content.forEach((key, value) {
    final methodName = key;
    if (value.toString().contains("{")) {
      keys.add('''
  String $methodName({int? pluralValue, List<String>? args, Map<String, dynamic>? namedArgs}) 
  => "$key".translate(pluralValue: pluralValue, args: args, namedArgs: namedArgs, getMapByLocale: (locale) => getMapByLocale(locale));''');
    } else {
      keys.add('''
  String get $methodName => "$key".translate(getMapByLocale: (locale) => getMapByLocale(locale));''');
    }
  });

  return ''' 
class $clazzName {

${keys.join('\n')}
}''';
}

String _fileFullPath(Map yaml, String fileName) {
  final outPutPath = yaml['output-path'] ?? "";

  if (outPutPath.isEmpty) {
    return "lib/$fileName";
  }

  if (!outPutPath.startsWith("lib/")) {
    return "lib/${outPutPath.first}/$fileName";
  }

  return "$outPutPath/$fileName";
}

final _file = '''//GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Internationalization
// **************************************************************************

import 'package:flutter/widgets.dart';
import 'package:flutter_json_localization/flutter_json_localization.dart';

class Intl {
  static String get stringsPath => "$_stringsPath";
  static List<Locale> get supportedLocales => $_suportedLocales;
  static List<String> get files => [\n${_filesName.join(',\n')}\n  ];

${_clazzInstances.join('\n')}
}


''';
