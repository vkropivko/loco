import 'package:flutter/material.dart';

import 'translator.dart';

extension StringExtension on String {
  String translate({
    int? pluralValue,
    List<String>? args,
    Map<String, dynamic>? namedArgs,
    Map<String, dynamic>? Function(Locale)? getMapByLocale
  }) {
    if (pluralValue != null) {
      final translation = Translator.instance?.pluralOf(
        this,
        pluralValue,
        args: args,
        namedArgs: namedArgs,
        getMapByLocale: getMapByLocale
      );

      if (translation != null && translation.isNotEmpty) {
        return translation;
      }
    }

    return Translator.instance!.valueOf(
        this,
        args: args,
        namedArgs: namedArgs,
        getMapByLocale: getMapByLocale
    );
  }
}