import 'package:flutter/widgets.dart';

import 'translator.dart';

class InternationalizationDelegate extends LocalizationsDelegate<Translator> {
  final String _translationsPath;
  final List<Locale> _suportedLocales;
  final List<String> _files;
  final BuildContext _context;

  InternationalizationDelegate({
    required String? translationsPath,
    required List<Locale>? suportedLocales,
    required List<String>? files,
    required BuildContext context
  })  : assert(
  translationsPath != null && translationsPath.isNotEmpty,
  "translationsPath can't be null or empty",
  ),
        assert(
        suportedLocales != null && suportedLocales.isNotEmpty,
        "suportedLocales can't be null or empty",
        ),
        assert(
        files != null && files.isNotEmpty,
        "files can't be null or empty",
        ),
        _translationsPath = translationsPath!,
        _suportedLocales = suportedLocales!,
        _files = files!,
        _context = context;

  @override
  bool isSupported(Locale locale) => _suportedLocales
      .map((locale) => locale.languageCode)
      .toList()
      .contains(locale.languageCode);

  @override
  Future<Translator> load(Locale locale) async {
    final translator = Translator.newInstance(
        _translationsPath,
        locale,
        _files,
        _context
    );
    return translator;
  }

  @override
  bool shouldReload(InternationalizationDelegate old) => false;
}
