import 'package:flutter/widgets.dart';
import 'package:flutter_json_localization/flutter_json_localization.dart';

class Translator {
  static late Translator _instance;

  final String _translationsPath;
  final Locale _locale;
  final List<String> _files;
  BuildContext context;

  Translator._(this._translationsPath, this._locale, this._files, this.context);

  factory Translator.newInstance(String translationsPath, Locale locale,
      List<String> files, BuildContext context) {
    _instance = Translator._(translationsPath, locale, files, context);

    return _instance;
  }

  static Translator? get instance {
    try {
      return Localizations.of<Translator>(Localization.context, Translator)!;
    } catch (_) {}

    return _instance;
  }

  String _interpolateValue(
    String value,
    List<String>? args,
    Map<String, dynamic>? namedArgs,
  ) {
    for (int i = 0; i < (args?.length ?? 0); i++) {
      value = value.replaceAll("{$i}", args![i]);
    }

    if (namedArgs?.isNotEmpty == true) {
      namedArgs?.forEach(
        (entryKey, entryValue) => value = value.replaceAll(
          "::$entryKey::",
          entryValue.toString(),
        ),
      );
    }

    return value;
  }

  String valueOf(String key,
      {List<String>? args,
      Map<String, dynamic>? namedArgs,
      Map<String, dynamic>? Function(Locale)? getMapByLocale}) {
    String? value = getMapByLocale?.call(_locale)?[key];
    if (value == null) return "";
    value = _interpolateValue(value, args, namedArgs);

    return value;
  }

  String pluralOf(String key, int pluralValue,
      {List<String>? args,
      Map<String, dynamic>? namedArgs,
      Map<String, dynamic>? Function(Locale)? getMapByLocale}) {
    Map<String, dynamic>? plurals = getMapByLocale?.call(_locale)?[key];
    if (plurals == null) return "";
    final plural = {0: "zero", 1: "one"}[pluralValue] ?? "other";
    String value = plurals[plural].toString();
    value = _interpolateValue(value, args, namedArgs);

    return value;
  }
}
